import React from 'react'

import './styles.css'

import whatsappIcon from '../../assets/images/icons/whatsapp.svg'

interface Props {
  name: string;
  subject: string;
  photo?: string;

  price: number;
  whatsapp?: string;
}

const TeacherItem: React.FC<Props> = ({name, subject, photo, price, whatsapp, children}) => {
  
  const formateCurrency = (value: number) => 
    value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })

  return (
    <article className="teacher-item">
      <header>
        <img src={photo} alt={name}/>
        <div>
          <strong>{name}</strong>
          <span>{subject}</span>
        </div>
      </header>
      {children}
      <footer>
        <p>
          Preço/hora
          <strong>{formateCurrency(price)}</strong>
        </p>
        <button type="button">
          <img src={whatsappIcon} alt="Whatsapp"/>
          Entrar em contato
        </button>
      </footer>
    </article>
  )
}

export default TeacherItem