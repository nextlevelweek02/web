import React from 'react'
import Router from './router'

import './assets/styles/colors.css'
import './assets/styles/global.css'

export default () => <Router />
