import React from 'react'
import { Link } from 'react-router-dom'

import PageHeader from '../../components/PageHeader/index'
import TeacherItem from '../../components/TeacherItem/index'

// import whatsappIcon from '../../assets/images/icons/whatsapp.svg'

import './styles.css'

export default () => {
  return (
    <div id="page-teacher-list" className="container">
      <PageHeader 
        // title="Que incrível que você quer dar aulas!" 
        title="Estes são os Proffys disponíveis." 
      >
        <form id="search-teachers">

          <div className="input-block">
            <label htmlFor="subject">Matéria</label>
            <input type="text" id="subject"/>
          </div>

          <div className="input-block">
            <label htmlFor="week_day">Dia da semana</label>
            <input type="text" id="week_day"/>
          </div>

          <div className="input-block">
            <label htmlFor="time">Horário</label>
            <input type="text" id="time"/>
          </div>

        </form>
      </PageHeader>

      <main>

        {
          [1,1,1,1,1].map(() => {
            return(
              <TeacherItem 
                name="Giovanni Pregnolato Rosim"
                subject="Programação Web"
                photo="https://gdev.netlify.app//media/eu_face.0c32afe3.png"
                price={60}
                whatsapp="5511995593220"
              >
              <p>
                Apaixonado por tecnologia e adora estar sempre aprendendo sobre as novidades da sua area.
                <br /><br />
                Gosta de compartilhar seu conhecimento e saber que ele esta ajudando as pessoas.
              </p>
              </TeacherItem>
            )
          })
        }

      </main>

    </div>
  )
}